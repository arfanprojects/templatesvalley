<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('my-products', 'ProductController@index');
Route::get('product/add', 'ProductController@addProduct');
Route::get('product/edit/{id}', 'ProductController@editProduct');
Route::post('product/save', 'ProductController@store');


Route::get('category/{id}', 'HomeController@category');
Route::get('category/{id}/{slug}', 'HomeController@category');
Route::get('product/{id}', 'HomeController@product');
Route::get('product/{id}/{slug}', 'HomeController@product');

Route::get('import_data', 'HomeController@import_data');



