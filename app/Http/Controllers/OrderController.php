<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB, Auth;
use App\Order;
class OrderController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth');
    }
	
	public function index() { 
		
	}
	
	public function addToCart(Request $request) {
		$order_id = Session::get("order_id");
		if(empty($order_id)) { 
			$newdata = array(
				//"user_id" => Auth->user()->id,
				"created_at" => date("Y-m-d h:i:s")
			);			
			
			$order_id = Order::insertGetId($newdata);
			Session::put("order_id" , $order_id);
		}
		$data = array(
			"user_id" => Auth()->user->id,
			"product_id" => $request->input("product_id"),
			"order_id" => $order_id,
			"created_at" => date("Y-m-d h:i:s")
		);
		
		DB::table("order_details")->insert($data);
		return 1;
	}
	
	public function paymentPage() { 
		return view("payment");
	}
	
	public function myOrders() { 
		
	}
	
}
