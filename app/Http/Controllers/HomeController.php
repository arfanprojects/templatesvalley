<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product,App\Category;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$featured_products = Product::orderBy("id" , "DESC")->paginate(17);
		/// Latest products 
		$latest_products = Product::limit(6)->orderBy("id" , "DESC")->get();
        return view('home' , ['featured_products' => $featured_products, 'latest_products' => $latest_products]);
    }
	
	public function category($category_id)
    {
		$category = Category::find($category_id);
		$products = Product::where("category_id" , $category_id)->paginate(17);
        return view('category' , ['products' => $products,'category' => $category]);
    }
	
	public function product($id)
    {
		$product = Product::find($id);
        return view('product' , ['product' => $product]);
    }
	
	public function AddProduct() { 
		return view("add_product");
	}
	
	public function import_data() { 
		$products = file_get_contents("data.json");
		$p_array = array();
		$products = json_decode($products);
		foreach($products->matches as $row) {
				if(!empty($row->previews->icon_with_landscape_preview->landscape_url)) {
					$preview = $row->previews->icon_with_landscape_preview->landscape_url;
				} else { 
					$preview = $row->previews->icon_with_video_preview->landscape_url;
				}
			$data = array(
				"title" => $row->name,
				"category_id" => 24,
				"price" => $row->price_cents/100,
				"number_of_sales" => $row->number_of_sales,
				"link" => $row->url,
				"language" => substr($row->summary,450),
				"updated_at" => date("Y-m-d" , strtotime($row->updated_at)),
				"created_at" => date("Y-m-d" , strtotime($row->published_at)),
				"description" => $row->description_html,
				"tags" => json_encode($row->tags),
				"previews" => $preview
			);
			
			Product::insert($data);
			
			//echo "<pre>"; print_r($data); exit;
		}
		echo "<pre>"; print_r($p_array); 
	}
}
