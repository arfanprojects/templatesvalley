<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Auth;
use Image;
use App\Category;
use Illuminate\Support\Facades\Input;
class ProductController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function index() { 
		$products = Product::where("user_id" , Auth::user()->id)->paginate(20);
		return view("products.home" , ['products' => $products]);
	}
	
	public function addProduct() {
		$categories = Category::where("parent_id" , 0)->get();
		foreach($categories as $parent) { 
			$parent->sub_cats = Category::where("parent_id" , $parent->id)->get();
		}
		return view("products.form" , compact('categories'));
	}
	
	public function store(Request $request) { 

		$data = array(
			"title" => $request->input('title'),
			"tags" => $request->input('tags'),
			"language" => $request->input('languages'),
			"link" => $request->input('link'),
			"download" => $request->input('download'),
			"category_id" => $request->input('category_id'),
			"description" => $request->input('description')
		);
		if($request->input('id')) { 
			$insert_id = $request->input('id');
			Product::where("id" , $insert_id)->update($data);
		} else  {
			$insert_id = Product::insertGetId($data);
		}
		
		$file_name = "templatesvalley-" . $insert_id . ".jpg";
		
		 $file = Input::file('main_file');
		if (Input::hasFile('main_file')) {
		
            $extension = $file->getClientOriginalExtension();
            $path = $file->storeAs("storage/products/", $file_name);
            $img = Image::make($file->getRealPath());
			 Product::where("id" , $insert_id)->update(array("image" => $file_name));
            $path2 = public_path("storage/products/$file_name"); 
            $img->fit(250)->save($path2);
        }
		
		return redirect("my-products");
		
		
		 
	}
	
	public function editProduct($id) { 
		$categories = Category::where("parent_id" , 0)->get();
		foreach($categories as $parent) { 
			$parent->sub_cats = Category::where("parent_id" , $parent->id)->get();
		}
		$product = Product::where("id" , $id)->first();
		return view("products.edit" , ['product' => $product , 'categories' => $categories]);
	}
}
