<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use DB;

class ApiController extends Controller
{
    public function getCompanies() { 
		$companies = DB::table("companies")->get();
		foreach($companies as $company) { 
			 $companydata = DB::table("records")->where("company_id" , $company->id)->orderBy("datetime" , "DESC")->first();
			 if(!empty($companydata->datetime)) { 
				  $datetime = $companydata->datetime; 
				  $companydata_last = DB::table("records")->where("company_id" , $company->id)->where("datetime" , "<", $datetime)->orderBy("datetime" , "DESC")->first();
				  
				  $companydata->change = number_format($companydata->last_day - $companydata_last->last_day , 2);
				  $companydata->percent = number_format(($companydata->last_day - $companydata_last->last_day)/$companydata_last->last_day * 100 , 2) . "%";
				  
				  $start_date = date("Y-m-d", strtotime("first day of month"));
				  $end_date = date("Y-m-d");
				  $perdiction_total = DB::table("records")->where("company_id" , $company->id)->where("datetime" , ">=", $start_date)->where("datetime" , "<=", $end_date)->get();
				  $perdiction = DB::table("records")->where("company_id" , $company->id)->where("datetime" , ">=", $start_date)->where("datetime" , "<=", $end_date)->sum("last_day");
					$perdiction_data = $perdiction/count($perdiction_total);
					$companydata->thismonth = $perdiction_data;
					if($perdiction_data > $companydata->last_day) {
						$companydata->perdiction = "Time to Hold";	
					} else { 
						$companydata->perdiction = "Time to Buy Company Share";
					}
			  
			  }
			  $company->data = $companydata;
			
		}
		
		
		return Response::json(
                array(
                        "status" => true,
						"companies" => $companies
                ), 200
            );
			
	}
	
	public function getCompanyDetail($id) { 
		$company = DB::table("companies")->where("id" , $id)->first();
		$company_data = DB::table("records")->where("company_id" , $id)->get();
		return Response::json(
                array(
                    "status" => true,
					"company" => $company,
					"data" => $company_data,
                ), 200
            );
			
	}
	
	public function PostData() {
		$companies = DB::table("companies")->get();
		 foreach($companies as $company) { 
		 $code = $company->dataset_code;
		 $id = $company->id;
		$url = "https://www.quandl.com/api/v3/datasets/PSX/$code.json?api_key=RH4GFD1yU43inakqVzsn";
		$result = file_get_contents($url);
        $parsed_json = json_decode($result);

        $result_data = $parsed_json->dataset->data;
		
		 foreach ($parsed_json->dataset->data as $key => $row) {
            $open = $row[1]; /// A
            $high = $row[2]; /// B
            $low = $row[3]; //// C
            $turnover = $row[4];// D
            $last_day = $row[5];// E
     
			
			$data = array(
                "datetime" => $row[0],
                "company_id" =>  $id,
                "open" => $open,
                "high" => $high,
                "low" => $low,
                "turnover" => $turnover,
                "last_day" => $last_day
			   );
			   
			// echo "<pre>"; print_r($data); 
			$record = DB::table("records")->where("company_id", $id)->where("datetime", $row[0])->first();
            if($record) {
                    DB::table("records")->where("id", $record->id)->update($data);
            } else {
                    DB::table("records")->insert($data);
            }
			
			   
		 }
		 }
	}
}
