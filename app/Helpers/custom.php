<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Auth;
use Session as s;

/*
 *  Get Simple Comapny 
 * param id
 */

function get_category($id) {
    $res = DB::table("category")->where("id" , $id)->first();
    if (count($res) <= 0)
        return false;
    return $res;
}

function clean($string) {
    $string = str_replace(' ', '-', strtolower($string)); // Replaces all spaces with hyphens.

    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function get_parent_categories() {
    $res = DB::table("category")->where("parent_id" , 0)->get();
    if (count($res) <= 0)
        return false;
    return $res;
}

function get_child_categories($id) {
    $res = DB::table("category")->where("parent_id" , $id)->get();
    if (count($res) <= 0)
        return false;
    return $res;
}


function get_product($id) {
    $res = DB::table("product")->where("id" , $id)->first();
    if (count($res) <= 0)
        return false;
    return $res;
}


function get_setting() {
    $res = DB::table("settings")->where("id" , 1)->first();
    if (count($res) <= 0)
        return false;
    return $res;
}


function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}





