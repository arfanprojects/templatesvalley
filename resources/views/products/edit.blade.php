@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="{{url('assets/editor/summernote.css')}}" type="text/css">
<script type="text/javascript" src="{{url('assets/editor/summernote.js')}}"></script>
<script> 
$(document).ready(function() {
  $('#description').summernote({
		height: 250
});
});

</script>
<br> 
<h4>Edit Product</h4>
													
				<form method="post" action="{{url('product/save')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
														<div class="row uniform">
															<div class="12u$">
																<input type="text" name="title" id="title" value="{{$product->title}}" placeholder="Title" />
															</div>
															
															<div class="6u">
																<input type="text" name="link" id="title"  placeholder="Demo Link" value="{{$product->link}}"/>
															</div>
															
															<div class="6u$">
																<input type="text" name="download" id="title" value="{{$product->download}}" placeholder="Download Link" />
															</div>
															
															<div class="6u">
																<input type="text" name="tags" id="title" value="{{$product->tags}}" placeholder="Tags" />
															</div>

															<div class="6u$">
																<input type="text" name="languages" id="title" value="{{$product->language}}" placeholder="Languages" />
															</div>
														
															<div class="6u">
																<div class="select-wrapper">
																	<select name="category_id" id="demo-category">
																		<option value="">- Category -</option>
																		@foreach($categories as $category)
																		 <optgroup label="{{$category->name}}">
																		 @foreach($category->sub_cats as $sub)
																			<option <?php if($product->category_id == $sub->id) { echo "selected"; } ?> value="{{$sub->id}}">{{$sub->name}}</option>
																			@endforeach
																		  </optgroup>
																		  @endforeach
																	</select>
																</div>
															</div>
															<div class="6u$">
																<input type="file" name="main_file" />
															</div>
															
															<!-- Break -->
															<div class="12u$">
																<textarea name="description" id="description" placeholder="Enter your message">{{$product->description}}</textarea>
															</div>
															<!-- Break -->
															<input type="hidden" value="{{$product->id}}" name="id" />
															<div class="12u$">
																<ul class="actions">
																	<li><input type="submit" value="Save" class="special" /></li>
																	<li><input type="reset" value="Reset" /></li>
																</ul>
															</div>
														</div>
													</form>				

@endsection