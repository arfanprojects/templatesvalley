@extends('layouts.app') @section('content')
<br>
<h4>Products
	<a class="button" href="{{url('product/add')}}">  Add New </a>
</h4>

<div class="table-wrapper">
	<table class="alt">
		<thead>
			<tr>
				<th>Name</th>
				<th>Description</th>
				<th>Options</th>
			</tr>
		</thead>
		<tbody>
			@foreach($products as $product)
			<tr>
				<td>{{$product->title}}</td>
				<td>{{$product->language}}</td>
				<td>
					<a href="{{url('product/edit/' . $product->id)}}"> Edit </a> |
					<a href="#"> Delete </a>
				</td>
			</tr>
			@endforeach

		</tbody>

	</table>
	{!! $products->render() !!}
</div>


@endsection