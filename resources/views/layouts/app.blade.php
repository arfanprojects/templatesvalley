<!DOCTYPE HTML>

<html>
	 @include('layouts.head')

	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								 @include('layouts.header')
								  @yield('content')
						</div>
					</div>

				 @include('layouts.menu')

			</div>

		<!-- Scripts -->
			
			<script src="{{url('assets/js/skel.min.js')}}"></script>
			<script src="{{url('assets/js/util.js')}}"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="{{url('assets/js/main.js')}}"></script>

			
	</body>
</html>