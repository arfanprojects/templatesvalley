<head>
		 <title>{{$title : "Templatesvalley"}}</title>
		 <meta name="csrf-token" content="{{ csrf_token() }}">
		 <meta charset="utf-8" />
		 <meta charset="UTF-8">
		  <meta name="description" content="Code and Script for Bootstrap, Javascript, PHP, Wordpress, HTML5 and more. Get Best items on templatesvalley">
		  <meta name="keywords" content="HTML,CSS,JavaScript, html templates, WordPress, wp, wp themes, html templates, scripts, php, php scripts">
		  <meta name="author" content="arfan67@gmail.com">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="{{url('assets/css/main.css')}}" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
	<script src="{{url('assets/js/jquery.min.js')}}"></script>
	</head>