<div id="sidebar">
						<div class="inner">
							<!-- Search -->
								<section id="search" class="alt">
									<form method="post" action="#">
										<input type="text" name="query" id="query" placeholder="Search" />
									</form>
								</section>

							<!-- Menu -->
								<nav id="menu">
									<header class="major">
										<h2>TemplatesValley</h2>
										<p>Scripts and Templates </p>
									</header>
									<ul>
										<li <?php if (Request::segment(1) == "" ) { ?> class="active" <?php } ?> ><a href="{{url('/')}}">Home</a></li>
										<?php $categories = get_parent_categories(); 
											foreach($categories as $row) : 
											
											$child_categories = get_child_categories($row->id);
											//print_r($child_categories);
							
											if(count($child_categories) > 1) { 
										?>
										<li>
											<span class="opener">{{$row->name}}</span>
											<ul>
												<?php foreach($child_categories as $child) : ?>
												<li <?php if (Request::segment(2) == $child->id ) { ?> class="active" <?php } ?>><a href="<?php echo url("category/" . $child->id . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-',$row->name)))); ?>">{{$child->name}}</a></li>
												<?php endforeach; ?>
											</ul>
										</li>
											<?php } else { ?>
												<li <?php if (Request::segment(2) == $row->id ) { ?> class="active" <?php } ?>><a href="<?php echo url("category/" . $row->id . "/" . strtolower(preg_replace('/[^A-Za-z0-9\-]/', '', str_replace(' ', '-',$row->name)))); ?>">{{$row->name}}</a></li>
											<?php } ?>
										
										<?php endforeach; ?>
										<?php if(!empty(Auth::user())) { ?>
										 <li>
											<span class="opener">My Profile</span>
											<ul>
												<li> <a href="{{url('my-products')}}"> My Products </a> </li>
												<li> <a href="{{url('logout')}}">  Logout </a> </li>
											</ul>
										</li> 
										<?php } else {  ?>
										<li> <a href="{{url('login')}}"> Login Here </a> </li>
										<?php } ?>
							
									</ul>
								</nav>

							<!-- Section -->
								

							<!-- Section -->
								<section>
									
									<ul class="contact">
										<li class="fa-envelope-o"><a href="#">arfan67@gmail.com</a></li>
										<li class="fa-phone">+92-300-5095213</li>
										<?php /* <li class="fa-home"></li> */ ?>
									</ul>
								</section>
								<section>
									
								</section>

							<!-- Footer -->
								<footer id="footer">
									<p class="copyright">&copy; <?php echo date("Y"); ?>  All rights reserved.  <a href="http:/.about.me/aliarfan">Arfan Ali</a> </p>
								</footer>

						</div>
					</div>
					
