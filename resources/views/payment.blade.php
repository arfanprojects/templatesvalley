@extends('layouts.app')

@section('content')

<script type="text/javascript" src="https://www.2checkout.com/checkout/api/2co.min.js"></script>
<script type="text/javascript">

  // Called when token created successfully.
  var successCallback = function(data) {
    var myForm = document.getElementById('myCCForm');

    // Set the token as the value for the token input
    myForm.token.value = data.response.token.token;

    // IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.
    myForm.submit();
  };

  // Called when token creation fails.
  var errorCallback = function(data) {
    if (data.errorCode === 200) {
      // This error code indicates that the ajax call failed. We recommend that you retry the token request.
    } else {
      alert(data.errorMsg);
    }
  };

  var tokenRequest = function() {
    // Setup token request arguments
    var args = {
      sellerId: "901333356",
      publishableKey: "589C8CFA-EE0A-4B9E-8F08-0776156D4B1C",
      ccNo: $("#ccNo").val(),
      cvv: $("#cvv").val(),
      expMonth: $("#expMonth").val(),
      expYear: $("#expYear").val()
    };
	console.log(args);
    // Make the token request
    TCO.requestToken(successCallback, errorCallback, args);
  };

  $(function() {
    // Pull in the public encryption key for our environment
    TCO.loadPubKey('sandbox');
	// TCO.loadPubKey('sandbox', function() {
		// TCO.requestToken(successCallback, errorCallback, args);
	// });​

    $("#myCCForm").submit(function(e) {
      // Call our token request function
      tokenRequest();

      // Prevent form from submitting
      return false;
    });
  });

</script>
		<section>
									<header class="main">
										<h1>Generic</h1>
									</header>
<div class="row">
											<div class="6u 12u$(small)">
											
											
											
											
											
	<form action="https://www.paypal.com/cgi-bin/webscr" method="post">

    <input type="hidden" value="_cart" name="cmd">
    <input type="hidden" value="1" name="upload">
    <input type="hidden" value="webguy040@gmail.com" name="business">
    <input type="hidden" value="1" name="item_id_1">
    <input type="hidden" value="test" name="item_name_1">

	<input type="hidden" value="20" id="amount_1" name="amount_1">
	
    <input type="hidden" value="1" name="quantity_1">
    <input type="hidden" value="arfan" name="custom">
    <input type="hidden" value="<?php echo url("home/purchase/"); ?>" name="notify_url">
    <input type="hidden" value="<?php echo url("home/paypal_payment"); ?>" name="return">
    <input type="hidden" value="2" name="rm">
    <input type="hidden" value="Return to The Store" name="cbt">
    <input type="hidden" value="<?php echo url("home/purchase/1"); ?>" name="cancel_return">
    <input type="hidden" value="USD" name="lc">
    <input type="hidden" value="USD" name="currency_code">
    <input type="image" alt="Make payments with PayPal - its fast, free and secure!" name="submit" src="http://www.paypal.com/en_US/i/btn/x-click-but01.gif">

</form>





											
											
											
											
									<form id="myCCForm" action="order.php"  method="post">
					      <!-- all other fields you want to collect, e.g. name and shipping address -->
					    	<div class="col-sm-4">
								<div class="widget-box">
									<div class="widget-header">
										<h5 class="widget-title">
											<div class="pull-right margin_r10">Amount: &pound;65</div>
											InvoiceNo: 32332
										</h5>
									</div>
									
									<div class="row uniform">
									
														<div class="12u 12u$(xsmall)">
														<label>Name on Card</label>
																<input id="name" type="text" value="" autocomplete="off" required  />
															</div>
															<div class="12u$ 12u$(xsmall)">
															<label>Card Number</label>
															<input id="ccNo" type="text" value="" autocomplete="off" required />
															</div>
															
															<div class="4u 12u$(xsmall)">
															<label>CVC</label>
																<input id="cvv" type="text" value="" autocomplete="off" required />
															</div>
														<div class="4u 12u$(xsmall)">
															<label>Expiry(MM)</label> 
															<input id="expMonth" type="text" size="2" required />

														</div>
														
														<div class="4u$ 12u$(xsmall)">
															<label>Expiry(YYYY)</label> 
															<input id="expYear" type="text" size="4" required />
														</div>
														<div class="4u$ 12u$(xsmall)">
														 <input type="hidden" name="amount" value="10" />
														  <input type="hidden" name="billing_id" value="123" />
														  <input type="submit" class="special" value="Make Payment" />
														  
														</div>
									</div>
									
								</div>
							</div>
					    </form>
					
					</div>
						<div class="6u 12u$(small)">
							<h3>Sem turpis amet semper</h3>
							<p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat commodo eu sed ante lacinia. Sapien a lorem in integer ornare praesent commodo adipiscing arcu in massa commodo lorem accumsan at odio massa ac ac. Semper adipiscing varius montes viverra nibh in adipiscing blandit tempus accumsan.</p>
						
						</div>
						</div>
								</section>
@endsection