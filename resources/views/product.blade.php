@extends('layouts.app')

@section('content')
<!-- Content -->

<style> 
#description img {
	height: auto !important;
	width: auto !important;
}
</style>
								<section>
									<header class="main">
										<h2>{{$product->title}}</h2>
									</header>
									
									<?php
										
										if(!empty($product->image)) { 
											$src = url('storage/products/' . $product->image);
										} else { 
											$src = $product->previews;
										}
										?>
<div class="row">
									<div class="8u 12u$(medium)">
									<span class="image main"><img src="{{$src}}" alt="" /></span>
											<div id="description"> 
											{!!$product->description!!}
											
											</div>
											
											
									
									<hr class="major" />

</div>
<div class="4u 12u$(medium)">
												<div class="box">
												<h3> Price : ${{$product->price}} </h3>
												<a target="_blank" href="{{$product->link}}?ref=marfan" class="button">Demo Here </a> 
												</div>
												
												<div class="box">{{$product->tags}}</div>
												<div class="box">{{$product->languages}}</div>
												
											</div>
								</section>
@endsection