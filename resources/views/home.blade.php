@extends('layouts.app')

@section('content')
<section>
									<!-- <header class="major">
										<h2>Featured Items</h2>
									</header> -->
									<div class="posts">
									@foreach($featured_products as $key=>$product)
										<?php if($key%3 == 0 and $key < 10 and $key != 0) { ?>
										<article>
											<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 255 text -->
<ins class="adsbygoogle"
     style="display:inline-block;width:250px;height:230px"
     data-ad-client="ca-pub-6456424833116352"
     data-ad-slot="9741668080"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
										</article>
										<?php
										} 
										if(!empty($product->image)) { 
											$src = url('storage/products/' . $product->image);
										} else { 
											$src = $product->previews;
										}
										?>
										
										<article>
											<a href="<?php echo url('product/' . $product->id . '/' . str_slug($product->title)); ?>" class="image">
											<img src="{{$src}}" alt="" /> <br>
											<h3>{{$product->title}}</h3>
											<p> Updated : {{date("M d,Y" , strtotime($product->created_at))}}
											</a>
										</article>
									@endforeach
									{{ $featured_products->render() }}
									</div>
								</section>
								

@endsection
